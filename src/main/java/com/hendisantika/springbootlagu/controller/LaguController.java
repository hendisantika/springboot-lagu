package com.hendisantika.springbootlagu.controller;

import com.hendisantika.springbootlagu.entity.Lagu;
import com.hendisantika.springbootlagu.service.LaguService;
import com.hendisantika.springbootlagu.util.ExcelGenerator;
import com.hendisantika.springbootlagu.util.GeneratePdfReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.ByteArrayInputStream;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-lagu
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/05/20
 * Time: 05.49
 */
@Controller
@RequestMapping(value = "/")
public class LaguController {

    @Autowired
    private LaguService laguService;

    @Autowired
    private ExcelGenerator excelGenerator;

    @GetMapping
    public String index(Model model) {
        model.addAttribute("lagus", laguService.getLagus());
        return "index";
    }

    @GetMapping(value = "tambah")
    public String showTambahForm(Model model) {
        model.addAttribute("lagu", new Lagu());
        return "tambah";
    }

    @PostMapping(value = "tambah")
    public String tambahLaguBaru(@ModelAttribute("lagu") Lagu lagu) {
        laguService.saveLagu(lagu);
        return "redirect:/";
    }

    @GetMapping(value = "edit/{id}")
    public String editLagu(Model model, @PathVariable("id") Integer id) {
        Lagu lagu = laguService.getLagu(id);
        model.addAttribute("lagu", lagu);
        return "edit";
    }

    @PostMapping(value = "edit")
    public String updateLagu(@ModelAttribute("lagu") Lagu lagu) {
        laguService.updateLagu(lagu);
        return "redirect:/";
    }

    @GetMapping(value = "delete/{id}")
    public String deletingLagu(@PathVariable("id") Integer id) {
        Lagu lagu = laguService.getLagu(id);
        laguService.deleteLagu(lagu);
        return "redirect:/";
    }

    @GetMapping(value = "pdf",
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> listLaguReport() {

        List<Lagu> laguList = laguService.getLagus();

        ByteArrayInputStream bis = GeneratePdfReport.listLagusReport(laguList);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=List-Lagu.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    @GetMapping("/xls")
    public ResponseEntity<InputStreamResource> excelLaguReport() throws Exception {
        List<Lagu> laguList = laguService.getLagus();

        ByteArrayInputStream in = excelGenerator.exportExcel(laguList);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=List-Lagu.xlsx");

        return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));

    }
}
