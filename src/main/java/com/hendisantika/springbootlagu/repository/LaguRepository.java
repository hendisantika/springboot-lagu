package com.hendisantika.springbootlagu.repository;

import com.hendisantika.springbootlagu.entity.Lagu;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-lagu
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/05/20
 * Time: 05.34
 */
public interface LaguRepository extends JpaRepository<Lagu, Integer> {
}
