package com.hendisantika.springbootlagu.service;

import com.hendisantika.springbootlagu.entity.Lagu;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-lagu
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/05/20
 * Time: 05.35
 */
public interface LaguService {
    List<Lagu> getLagus();

    Lagu getLagu(Integer id);

    Lagu saveLagu(Lagu lagu);

    Lagu updateLagu(Lagu lagu);

    void deleteLagu(Lagu lagu);
}
