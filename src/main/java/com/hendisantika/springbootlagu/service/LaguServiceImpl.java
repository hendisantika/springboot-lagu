package com.hendisantika.springbootlagu.service;

import com.hendisantika.springbootlagu.entity.Lagu;
import com.hendisantika.springbootlagu.exception.ResourceNotFoundException;
import com.hendisantika.springbootlagu.repository.LaguRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-lagu
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/05/20
 * Time: 05.36
 */
@Service
@Transactional
public class LaguServiceImpl implements LaguService {
    @Autowired
    private LaguRepository laguRepository;

    @Override
    public List<Lagu> getLagus() {
        return laguRepository.findAll();
    }

    @Override
    public Lagu getLagu(Integer id) {
        return laguRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Lagu [laguId=" + id + "] " +
                "can't be found"));
    }

    @Override
    public Lagu saveLagu(Lagu lagu) {
        return laguRepository.save(lagu);
    }

    @Override
    public Lagu updateLagu(Lagu lagu) {
        return laguRepository.findById(lagu.getId()).map(laguTemp -> {
            laguTemp.setId(lagu.getId());
            laguTemp.setJudul(lagu.getJudul());
            laguTemp.setPenyanyi(lagu.getPenyanyi());
            laguTemp.setPencipta(lagu.getPencipta());
            laguRepository.save(laguTemp);
            return laguTemp;
        }).orElseThrow(() -> new ResourceNotFoundException("Lagu [laguId=" + lagu.getId() + "] can't be found"));
    }

    @Override
    public void deleteLagu(Lagu lagu) {
        laguRepository.findById(lagu.getId()).orElseThrow(() -> new ResourceNotFoundException("Lagu " +
                "[laguId=" + lagu.getId() + "] can't be found"));
        laguRepository.deleteById(lagu.getId());
    }
}
