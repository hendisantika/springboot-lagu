package com.hendisantika.springbootlagu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootLaguApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootLaguApplication.class, args);
    }

}
