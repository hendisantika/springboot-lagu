# springboot-lagu
Repository to show on how to Export to Excel & PDF File
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-lagu`
2. Go inside the folder: `cd springboot-lagu`
3. Run the application: `mvn clean spring-boot:run`
4. Open your favorite browser: http://localhost:8080

### Screen shot

Add New Song

![Add New Song](img/add.png "Add New Song")

List All Songs

![List All Songs](img/list.png "List All Songs")

Edit Song

![Edit Song](img/edit.png "Edit Song")

Export to PDF File

![Export to PDF File](img/pdf.png "Export to PDF File")
